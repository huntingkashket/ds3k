object fmEdit: TForm
  Left = 10
  Top = 10
  HelpType = htKeyword
  HelpKeyword = 
    'AAAAAhQCEQVDTEFTUxEFVEZvcm0RBlBBUkFNUxQGEQhhdmlzaWJsZQURDGRvdWJs' +
    'ZWJ1ZmZlcgQRAXgGChEBeQYKEQF3CAJIEQFoCAGg'
  BorderIcons = []
  BorderStyle = bsNone
  Caption = '{Scripts master}'
  ClientHeight = 416
  ClientWidth = 584
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = fmMain.editorPopup
  Position = poDesigned
  Visible = False
  DesignSize = (
    584
    416)
  PixelsPerInch = 96
  TextHeight = 13
  object shape1: TShape
    Left = 0
    Top = 31
    Width = 584
    Height = 384
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcMQGIAAAAAAAARAWgMQFoAAAAAAAARBnBhcmVudBcFVEZvcm0UBhEK' +
      'Y2xhc3NfbmFtZQ4IEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMU' +
      'Aw4JDgsRBHNlbGYKBMKz2BEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250' +
      'AA4MCgDGq3AODRQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0EQfU' +
      '6OPz8OAxEQhwZW5jb2xvcgoAMCIfEQpicnVzaGNvbG9yCgAwIh8RCHBlbnN0eWxl' +
      'BgARCmJydXNoc3R5bGUGAREBeRECMzE='
    Brush.Style = bsClear
    Pen.Color = 3154463
  end
  object smp_fon: TShape
    Left = 0
    Top = 0
    Width = 584
    Height = 32
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUChEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzUwOREBaBECMzIRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3NfbmFt' +
      'ZQ4KEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4LDg0RBHNl' +
      'bGYKBMIomBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4OCgDGruAO' +
      'DxQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREIcGVuY29sb3IK' +
      'ADAiHxEKYnJ1c2hjb2xvcgoAs5+XEQF4BggRAXkGCA=='
    Align = alTop
    Brush.Color = 11771799
    Pen.Color = 3154463
    ExplicitWidth = 581
  end
  object clacbtn1: TLabel
    Left = 9
    Top = 367
    Width = 131
    Height = 35
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUDxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAWkGABEJZm91cmNvbG9yCgDe2NMRCnRocmVlY29sb3IKAMzBuxEIdHdv' +
      'Y29sb3IKANbOyREIb25lY29sb3IKAMi7txEBdwxAYgAAAAAAABEBaAxARAAAAAAA' +
      'ABEGcGFyZW50FwVURm9ybRQGEQpjbGFzc19uYW1lDg0RDwAqAF9jb25zdHJhaW50' +
      'cxcQVFNpemVDb25zdHJhaW50cxQDDg4OEBEEc2VsZgoGFnooEQgAKgBwcm9wcxQA' +
      'EQcAKgBpY29uABEIACoAX2ZvbnQADhEKAlmn4A4SFAIOAwURCnBvc2l0aW9uZXgR' +
      'CnBvRGVzaWduZWQRBHRleHQRCHtDYW5jZWx9EQpmbW91c2Vkb3duERZteURlc2ln' +
      'bjo6b2JqTW91c2VEb3duEQlmb250Y29sb3IKAGtaVhEBeAa5EQF5CAE/'
    Alignment = taCenter
    AutoSize = False
    Caption = '{Add}'
    Color = 14604243
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 7035478
    Font.Height = -15
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = False
    Layout = tlCenter
  end
  object clac1: TShape
    Left = 9
    Top = 402
    Width = 131
    Height = 2
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUEEQhhZW5h' +
      'YmxlZAURAXcMQGIAAAAAAAARAWgRATIRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3Nf' +
      'bmFtZQ4JEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4KDgwR' +
      'BHNlbGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4NCgJZ' +
      'p+AODhQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREKYnJ1c2hj' +
      'b2xvcgoA3rUAEQhwZW5jb2xvcgoA3rYAEQF4BrkRAXkIAWoRB2NhcHRpb24RCMfg' +
      '7OXt6PL8'
    Brush.Color = 14595328
    Pen.Color = 14595584
  end
  object clacbtn2: TLabel
    Left = 153
    Top = 367
    Width = 131
    Height = 35
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUDxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAWkGABEJZm91cmNvbG9yCgDe2NMRCnRocmVlY29sb3IKAMzBuxEIdHdv' +
      'Y29sb3IKANbOyREIb25lY29sb3IKAMi7txEBdwxAYgAAAAAAABEBaAxARAAAAAAA' +
      'ABEGcGFyZW50FwVURm9ybRQGEQpjbGFzc19uYW1lDg0RDwAqAF9jb25zdHJhaW50' +
      'cxcQVFNpemVDb25zdHJhaW50cxQDDg4OEBEEc2VsZgoGFnooEQgAKgBwcm9wcxQA' +
      'EQcAKgBpY29uABEIACoAX2ZvbnQADhEKAlmn4A4SFAIOAwURCnBvc2l0aW9uZXgR' +
      'CnBvRGVzaWduZWQRBHRleHQRCHtDYW5jZWx9EQpmbW91c2Vkb3duERZteURlc2ln' +
      'bjo6b2JqTW91c2VEb3duEQlmb250Y29sb3IKAGtaVhEBeAYREQF5CAEf'
    Alignment = taCenter
    AutoSize = False
    Caption = '{Edit}'
    Color = 14604243
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 7035478
    Font.Height = -15
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = False
    Layout = tlCenter
  end
  object clac2: TShape
    Left = 153
    Top = 402
    Width = 131
    Height = 2
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUEEQhhZW5h' +
      'YmxlZAURAXcMQGIAAAAAAAARAWgRATIRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3Nf' +
      'bmFtZQ4JEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4KDgwR' +
      'BHNlbGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4NCgJZ' +
      'p+AODhQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREKYnJ1c2hj' +
      'b2xvcgoA3rUAEQhwZW5jb2xvcgoA3rYAEQF4BhERAXkIAUIRB2NhcHRpb24RBntF' +
      'ZGl0fQ=='
    Brush.Color = 14595328
    Pen.Color = 14595584
  end
  object clacbtn3: TLabel
    Left = 297
    Top = 367
    Width = 131
    Height = 35
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUDxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAWkGABEJZm91cmNvbG9yCgDe2NMRCnRocmVlY29sb3IKAMzBuxEIdHdv' +
      'Y29sb3IKANbOyREIb25lY29sb3IKAMi7txEBdwxAYgAAAAAAABEBaAxARAAAAAAA' +
      'ABEGcGFyZW50FwVURm9ybRQGEQpjbGFzc19uYW1lDg0RDwAqAF9jb25zdHJhaW50' +
      'cxcQVFNpemVDb25zdHJhaW50cxQDDg4OEBEEc2VsZgoGFnooEQgAKgBwcm9wcxQA' +
      'EQcAKgBpY29uABEIACoAX2ZvbnQADhEKAlmn4A4SFAIOAwURCnBvc2l0aW9uZXgR' +
      'CnBvRGVzaWduZWQRBHRleHQRCHtDYW5jZWx9EQpmbW91c2Vkb3duERZteURlc2ln' +
      'bjo6b2JqTW91c2VEb3duEQlmb250Y29sb3IKAGtaVhEBeAahEQF5CAEf'
    Alignment = taCenter
    AutoSize = False
    Caption = '{Delete}'
    Color = 14604243
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 7035478
    Font.Height = -15
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = False
    Layout = tlCenter
  end
  object clac3: TShape
    Left = 297
    Top = 402
    Width = 131
    Height = 2
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUEEQhhZW5h' +
      'YmxlZAURAXcMQGIAAAAAAAARAWgRATIRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3Nf' +
      'bmFtZQ4JEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4KDgwR' +
      'BHNlbGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4NCgJZ' +
      'p+AODhQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREKYnJ1c2hj' +
      'b2xvcgoA3rUAEQhwZW5jb2xvcgoA3rYAEQF4BqERAXkIAUIRB2NhcHRpb24RBntF' +
      'ZGl0fQ=='
    Brush.Color = 14595328
    Pen.Color = 14595584
  end
  object clacbtn4: TLabel
    Left = 441
    Top = 367
    Width = 131
    Height = 35
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUDxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAWkGABEJZm91cmNvbG9yCgDe2NMRCnRocmVlY29sb3IKAMzBuxEIdHdv' +
      'Y29sb3IKANbOyREIb25lY29sb3IKAMi7txEBdwxAYgAAAAAAABEBaAxARAAAAAAA' +
      'ABEGcGFyZW50FwVURm9ybRQGEQpjbGFzc19uYW1lDg0RDwAqAF9jb25zdHJhaW50' +
      'cxcQVFNpemVDb25zdHJhaW50cxQDDg4OEBEEc2VsZgoGFnooEQgAKgBwcm9wcxQA' +
      'EQcAKgBpY29uABEIACoAX2ZvbnQADhEKAlmn4A4SFAIOAwURCnBvc2l0aW9uZXgR' +
      'CnBvRGVzaWduZWQRBHRleHQRCHtDYW5jZWx9EQpmbW91c2Vkb3duERZteURlc2ln' +
      'bjo6b2JqTW91c2VEb3duEQlmb250Y29sb3IKAGtaVhEBeAgBMREBeQgBHw=='
    Alignment = taCenter
    AutoSize = False
    Caption = '{OK}'
    Color = 14604243
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 7035478
    Font.Height = -15
    Font.Name = 'Segoe UI Semibold'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = False
    Layout = tlCenter
  end
  object clac4: TShape
    Left = 441
    Top = 402
    Width = 131
    Height = 2
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUEEQhhZW5h' +
      'YmxlZAURAXcMQGIAAAAAAAARAWgRATIRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3Nf' +
      'bmFtZQ4JEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4KDgwR' +
      'BHNlbGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4NCgJZ' +
      'p+AODhQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREKYnJ1c2hj' +
      'b2xvcgoA3rUAEQhwZW5jb2xvcgoA3rYAEQF4CAExEQF5CAFCEQdjYXB0aW9uEQZ7' +
      'RWRpdH0='
    Brush.Color = 14595328
    Pen.Color = 14595584
  end
  object smpl_l2: TLabel
    Left = 8
    Top = 46
    Width = 414
    Height = 19
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUCREIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzQ4NhEBaAxAOAAAAAAAABEGcGFyZW50FwVURm9ybRQGEQpjbGFz' +
      'c19uYW1lDgkRDwAqAF9jb25zdHJhaW50cxcQVFNpemVDb25zdHJhaW50cxQDDgoO' +
      'DBEEc2VsZgoGFnooEQgAKgBwcm9wcxQAEQcAKgBpY29uABEIACoAX2ZvbnQADg0K' +
      'Almn4A4OFAIOAwURCnBvc2l0aW9uZXgRCnBvRGVzaWduZWQRBHRleHQRB3tGaW5k' +
      'On0RCWZvbnRjb2xvcgoAenp6EQF4BjgRAXkGNg=='
    AutoSize = False
    Caption = '{Program scripts:}'
    Color = 2763306
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 8026746
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object smpl_desc: TLabel
    Left = 8
    Top = 310
    Width = 566
    Height = 43
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUCREIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzQ4NhEBaAxAOAAAAAAAABEGcGFyZW50FwVURm9ybRQGEQpjbGFz' +
      'c19uYW1lDgkRDwAqAF9jb25zdHJhaW50cxcQVFNpemVDb25zdHJhaW50cxQDDgoO' +
      'DBEEc2VsZgoGFnooEQgAKgBwcm9wcxQAEQcAKgBpY29uABEIACoAX2ZvbnQADg0K' +
      'Almn4A4OFAIOAwURCnBvc2l0aW9uZXgRCnBvRGVzaWduZWQRBHRleHQRB3tGaW5k' +
      'On0RCWZvbnRjb2xvcgoAenp6EQF4BhARAXkRAzMxMA=='
    Alignment = taCenter
    AutoSize = False
    Caption = 
      #1042#1089#1077' '#1076#1086#1073#1072#1074#1083#1077#1085#1085#1099#1077' '#1089#1082#1088#1080#1087#1090#1099' '#1085#1072#1093#1086#1076#1103#1090#1089#1103' '#1074' '#1087#1072#1087#1082#1077' '#1087#1088#1086#1077#1082#1090#1072' /scripts/.'#13#10#1048' ' +
      #1072#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080' '#1087#1088#1080#1082#1088#1077#1087#1083#1103#1102#1090#1089#1103' '#1082'  '#1092#1072#1081#1083#1091' '#1087#1088#1086#1077#1082#1090#1072'.        '
    Color = 2763306
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 8026746
    Font.Height = -16
    Font.Name = 'Segoe UI Light'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    WordWrap = True
  end
  object smp_capt: TLabel
    Left = 8
    Top = 0
    Width = 542
    Height = 30
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUCREIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzQ4NhEBaBECMzARBnBhcmVudBcFVEZvcm0UBhEKY2xhc3NfbmFt' +
      'ZQ4KEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4LDg0RBHNl' +
      'bGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4OCgJZp+AO' +
      'DxQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0ERV7RmFzdCBSZXBs' +
      'YWNyIG1hc2hlcn0RCWZvbnRjb2xvcgoAjGIvEQF4EQE4EQF5EQEw'
    AutoSize = False
    Caption = '{Scripts master}'
    Color = 2763306
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 9200175
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Layout = tlCenter
  end
  object l_moves: TLabel
    Left = 0
    Top = 0
    Width = 552
    Height = 29
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVExhYmVsEQZQQVJBTVMUCREIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzU1MhEBaBECMjkRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3NfbmFt' +
      'ZQ4KEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4LDg0RBHNl' +
      'bGYKBhZ6KBEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4OCgJZp+AO' +
      'DxQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREJZm9udGNvbG9y' +
      'CgCMYi8RAXgGCBEBeQYg'
    AutoSize = False
    Color = 2763306
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = 9200175
    Font.Height = -19
    Font.Name = 'Segoe UI Light'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object frepl_close: TMImage
    Left = 550
    Top = 0
    Width = 32
    Height = 32
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEHVE1JbWFnZREGUEFSQU1TFAgRCGF2aXNpYmxlBREIYWVu' +
      'YWJsZWQFEQF3EQIzMhEBaAxAWgAAAAAAABEGcGFyZW50FwVURm9ybRQGEQpjbGFz' +
      'c19uYW1lDgkRDwAqAF9jb25zdHJhaW50cxcQVFNpemVDb25zdHJhaW50cxQDDgoO' +
      'DBEEc2VsZgoF6f/oEQgAKgBwcm9wcxQAEQcAKgBpY29uABEIACoAX2ZvbnQADg0K' +
      'Akl3wA4OFAIOAwURCnBvc2l0aW9uZXgRCnBvRGVzaWduZWQRBHRleHQNEQF4EQM1' +
      'NTARAXkGCA=='
    Picture.Data = {
      0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000003200
      00003208060000001E3F88B1000001344944415478DAEDD9ED0D82400C066098
      40477104DD4427934D740447D10D2C099718A3E5DABEBD02774DFACFF4FA10E1
      BEFA6E23D14737D0200DB2F0A80A72987208EAF14CF998520D190137CA3DE525
      003322AE944FCA1387E1209F88142531099182C5F482422531E2B1E7FE5A1118
      D598392F7B498C7AACDCCF6F098C690CC93CE28931D7964E881E18484DCDCC8E
      C4C06A6997288806900FC4B4D6B23402455821DA86E0080444DA980B0205C96D
      D00D8184CC35DA7922D0100EF32BA04B1C8F1D620E06BE4EF3DAEA7218979573
      8328116E98F6B20B10ABFAFC728841F09B5088A441374C5B341A1B8263AADE58
      211B80D5AAF2F0C10301AB5DD5015D098479AC2A0EB12310EAB1E72E7AEE94BB
      C2887F9817E5B1535CF47C6322AFDE58440E2461567F19BA9A6890A545832C2D
      36037903F5398C333A0E38DA0000000049454E44AE426082}
    Proportional = True
    Stretch = True
  end
  object e_r_fon: TShape
    Left = 8
    Top = 74
    Width = 566
    Height = 228
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEGVFNoYXBlEQZQQVJBTVMUCxEIYXZpc2libGUFEQhhZW5h' +
      'YmxlZAURAXcRAzQ0NhEBaBECNDQRBnBhcmVudBcFVEZvcm0UBhEKY2xhc3NfbmFt' +
      'ZQ4KEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMUAw4LDg0RBHNl' +
      'bGYKBcXu2BEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250AA4OCgIIpHAO' +
      'DxQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREIcGVud2lkdGgR' +
      'ATIRCHBlbmNvbG9yCgB6enoRCmJydXNoY29sb3IKANHIwhEBeQaqEQF4Big='
    Anchors = [akTop, akRight]
    Brush.Color = 13748418
    Pen.Color = 8026746
    Pen.Width = 2
  end
  object listBox1: TListBox
    Left = 16
    Top = 80
    Width = 552
    Height = 216
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEIVExpc3RCb3gRBlBBUkFNUxQFEQhhdmlzaWJsZQURCGFl' +
      'bmFibGVkBREEdGV4dA0RAXcMQGQAAAAAAAARAWgMQGQAAAAAAAA='
    Style = lbOwnerDrawFixed
    BorderStyle = bsNone
    Color = 13748418
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 0
    Alignment = taLeftJustify
    BorderSelected = True
    TwoColor = clNone
    TwoFontColor = clNone
    MarginLeft = 2
    ReadOnly = False
  end
  object do_add: __TNoVisual
    Left = 240
    Top = 48
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEJVEZ1bmN0aW9uEQZQQVJBTVMUDBEIYXZpc2libGUFEQhz' +
      'aG93aGludAQRCHByaW9yaXR5BgARCnRvcmVnaXN0ZXIFEQF4BvARAXkGCBEIYWVu' +
      'YWJsZWQFEQlpY29uX2ZpbGURQ0U6L1Byb2plY3RzL011bHRpbWVkaWEvTWVzcyBC' +
      'b3ggUEhQL3N5c3RlbS8vaW1hZ2VzLzI0L1RGdW5jdGlvbi5wbmcRBHRleHQNEQVw' +
      'YW5lbAoG+26gEQVsYWJlbA0RA29iag0='
    realWidth = 25
    realHeight = 25
  end
  object do_edit: __TNoVisual
    Left = 288
    Top = 48
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEJVEZ1bmN0aW9uEQZQQVJBTVMUDBEIYXZpc2libGUFEQhz' +
      'aG93aGludAQRCHByaW9yaXR5BgARCnRvcmVnaXN0ZXIFEQF4CAEgEQF5BggRCGFl' +
      'bmFibGVkBREJaWNvbl9maWxlEUNFOi9Qcm9qZWN0cy9NdWx0aW1lZGlhL01lc3Mg' +
      'Qm94IFBIUC9zeXN0ZW0vL2ltYWdlcy8yNC9URnVuY3Rpb24ucG5nEQR0ZXh0DREF' +
      'cGFuZWwKBvt5IBEFbGFiZWwNEQNvYmoN'
    realWidth = 25
    realHeight = 25
  end
  object do_delete: __TNoVisual
    Left = 344
    Top = 48
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEJVEZ1bmN0aW9uEQZQQVJBTVMUDBEIYXZpc2libGUFEQhz' +
      'aG93aGludAQRCHByaW9yaXR5BgARCnRvcmVnaXN0ZXIFEQF4CAFYEQF5BggRCGFl' +
      'bmFibGVkBREJaWNvbl9maWxlEUNFOi9Qcm9qZWN0cy9NdWx0aW1lZGlhL01lc3Mg' +
      'Qm94IFBIUC9zeXN0ZW0vL2ltYWdlcy8yNC9URnVuY3Rpb24ucG5nEQR0ZXh0DREF' +
      'cGFuZWwKBvtcQBEFbGFiZWwNEQNvYmoN'
    realWidth = 25
    realHeight = 25
  end
  object openDlg: __TNoVisual
    Left = 176
    Top = 48
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxENVE9wZW5EaWFsb2dFeBEGUEFSQU1TFAwRCGF2aXNpYmxl' +
      'BREIc2hvd2hpbnQEEQZmaWx0ZXIRGVBIUCBTY3JpcHRzICgqLnBocCl8Ki5waHAR' +
      'C2ZpbHRlcmluZGV4BgERAXgGsBEBeQYIEQhhZW5hYmxlZAURCWljb25fZmlsZRFH' +
      'RTovUHJvamVjdHMvTXVsdGltZWRpYS9NZXNzIEJveCBQSFAvc3lzdGVtLy9pbWFn' +
      'ZXMvMjQvVE9wZW5EaWFsb2dFeC5wbmcRBHRleHQNEQVwYW5lbAoG+2bAEQVsYWJl' +
      'bA0RA29iag0='
    realWidth = 25
    realHeight = 25
  end
  object sl_updateList: __TNoVisual
    Left = 248
    Top = 120
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEJVEZ1bmN0aW9uEQZQQVJBTVMUDBEIYXZpc2libGUFEQhz' +
      'aG93aGludAQRCHByaW9yaXR5BgARCnRvcmVnaXN0ZXIFEQF4BvgRAXkGUBEIYWVu' +
      'YWJsZWQFEQlpY29uX2ZpbGURQ0U6L1Byb2plY3RzL011bHRpbWVkaWEvTWVzcyBC' +
      'b3ggUEhQL3N5c3RlbS8vaW1hZ2VzLzI0L1RGdW5jdGlvbi5wbmcRBHRleHQNEQVw' +
      'YW5lbAoG+3FAEQVsYWJlbA0RA29iag0='
    realWidth = 25
    realHeight = 25
  end
  object timer1: __TNoVisual
    Left = 336
    Top = 120
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEKVEZ1bmNUaW1lchEGUEFSQU1TFA8RCGF2aXNpYmxlBREI' +
      'c2hvd2hpbnQEEQpiYWNrZ3JvdW5kBgARCHByaW9yaXR5EQZ0cElkbGURCGludGVy' +
      'dmFsEQMxMDARBmVuYWJsZQQRAXgIAVARAXkGUBEIYWVuYWJsZWQFEQlpY29uX2Zp' +
      'bGURREU6L1Byb2plY3RzL011bHRpbWVkaWEvTWVzcyBCb3ggUEhQL3N5c3RlbS8v' +
      'aW1hZ2VzLzI0L1RGdW5jVGltZXIucG5nEQR0ZXh0DREFcGFuZWwKBvt7wBEFbGFi' +
      'ZWwNEQZyZXBlYXQFEQNvYmoN'
    realWidth = 25
    realHeight = 25
  end
  object popupMenu1: __TNoVisual
    Left = 424
    Top = 72
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEMVFBvcHVwTWVudUV4EQZQQVJBTVMUDREIYXZpc2libGUF' +
      'EQhzaG93aGludAQRAXgIAagRAXkGIBEIYWVuYWJsZWQFEQlpY29uX2ZpbGURRkU6' +
      'L1Byb2plY3RzL011bHRpbWVkaWEvTWVzcyBCb3ggUEhQL3N5c3RlbS8vaW1hZ2Vz' +
      'LzI0L1RQb3B1cE1lbnVFeC5wbmcRBHRleHQNEQVwYW5lbAoG+2QgEQVsYWJlbA0R' +
      'BGRhdGERP8Tu4eDi6PL8IFtkb19hZGRdDQrQ5eTg6vLo8O7i4PL8IFtkb19lZGl0' +
      'XQ0K0+Tg6+jy/CBbZG9fZGVsZXRlXREHb2JqZWN0cxEUc2NyaXB0TGlzdC0+bGlz' +
      'dEJveDERBnN0eWxlZAURA29iag0='
    realWidth = 25
    realHeight = 25
  end
  object move_m_repls: __TNoVisual
    Left = 304
    Top = 264
    Width = 25
    Height = 25
    HelpType = htKeyword
    HelpKeyword = 
      'AAAAAhQCEQVDTEFTUxEKVEZ1bmNUaW1lchEGUEFSQU1TFBARCGF2aXNpYmxlBREI' +
      'c2hvd2hpbnQEEQphZXZpc2lhYmxlBBEEZmlsZQ0RCmJhY2tncm91bmQGABEIcHJp' +
      'b3JpdHkGAxEIaW50ZXJ2YWwRATERBmVuYWJsZQQRBnBhcmVudBcFVEZvcm0UBhEK' +
      'Y2xhc3NfbmFtZQ4NEQ8AKgBfY29uc3RyYWludHMXEFRTaXplQ29uc3RyYWludHMU' +
      'Aw4ODhARBHNlbGYKBMrIABEIACoAcHJvcHMUABEHACoAaWNvbgARCAAqAF9mb250' +
      'AA4RCgDGruAOEhQCDgMFEQpwb3NpdGlvbmV4EQpwb0Rlc2lnbmVkEQR0ZXh0DREL' +
      'b25tb3VzZWRvd24RFm15RGVzaWduOjpvYmpNb3VzZURvd24RBnJlcGVhdAURBWxh' +
      'YmVsDREDb2JqDREBeAgBMBEBeQgBCA=='
    realWidth = 25
    realHeight = 25
  end
end
